package com.volkswagen.fota.business;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

public final class PageCreator {

    private PageCreator() {}

    public static <T> Page<T> of(final Pageable pageable, final List<T> context) {
        return new PageImpl<>(context, pageable, context.size());
    }
}
