package com.volkswagen.fota.business.vehicle.entity;

import com.volkswagen.fota.business.feature.entity.Feature;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

@Entity
@Data
public class Vehicle {

    @Id
    private String vin;

    @ManyToMany(targetEntity = Feature.class, cascade = CascadeType.ALL)
    private Set<Feature> features;
}
