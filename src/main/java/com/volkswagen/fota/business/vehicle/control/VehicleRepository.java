package com.volkswagen.fota.business.vehicle.control;

import com.volkswagen.fota.business.feature.entity.Feature;
import com.volkswagen.fota.business.vehicle.entity.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleRepository extends JpaRepository<Vehicle, String> {

    Page<Vehicle> findVehicleByFeaturesIsNotContaining(final Feature feature, Pageable pageable);
    Page<Vehicle> findVehicleByFeaturesContaining(final Feature feature, Pageable pageable);
}
