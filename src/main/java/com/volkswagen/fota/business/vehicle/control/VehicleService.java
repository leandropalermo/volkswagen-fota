package com.volkswagen.fota.business.vehicle.control;

import com.volkswagen.fota.business.feature.control.FeatureDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface VehicleService {

    /**
     * Find All Vehicles registered into the database.
     *
     * @param pageRequest the pageRequest parameter
     * @return {@Page} of {@VehicleDto}
     */
    Page<VehicleDto> getVehicles(final PageRequest pageRequest);

    /**
     * Find all Features associated to a specific vin received by parameter.
     *
     * @param vin the vin parameter
     * @param pageRequest the pageRequest parameter
     * @return [@Page} of {@FeatureDto}
     */
    Page<FeatureDto> getFeatures(final String vin, final PageRequest pageRequest);

    /**
     * Find all incompatible Feature to a specific vin received by parameter.
     * It looks for all Features who do not have any association to the vin.
     *
     * @param vin the vin parameter
     * @param pageRequest the pageRequest paramter
     * @return [@Page} of {@FeatureDto}
     */
    Page<FeatureDto> getIncompatibleFeatures(final String vin, final PageRequest pageRequest);

    /**
     * Find all installable Feature to a specific vin received by parameter.
     * It looks for all Features who have some association to the vin.
     *
     * @param vin the vin parameter
     * @param pageRequest the pageRequest paramter
     * @return [@Page} of {@FeatureDto}
     */
    Page<FeatureDto> getInstallableFeatures(final String vin, final PageRequest pageRequest);
}
