package com.volkswagen.fota.business.vehicle.control;

import com.volkswagen.fota.business.PageCreator;
import com.volkswagen.fota.business.feature.control.FeatureDto;
import com.volkswagen.fota.business.feature.control.FeatureRepository;
import com.volkswagen.fota.business.feature.control.FeatureToFeatureDtoConverter;
import com.volkswagen.fota.business.feature.entity.Feature;
import com.volkswagen.fota.business.vehicle.entity.Vehicle;
import com.volkswagen.fota.exception.DataNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class VehicleServiceImpl implements VehicleService {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private FeatureRepository featureRepository;

    private static final String VEHICLE_NOT_FOUND_MESSAGE = "Was not possible to find the Vehicle %s.";
    private static final Boolean INSTALLABLE = Boolean.TRUE;
    private static final Boolean NOT_INSTALLABLE = Boolean.FALSE;

    @Override
    public Page<VehicleDto> getVehicles(final PageRequest pageRequest) {
        final Page<Vehicle> vehicles = this.vehicleRepository.findAll(pageRequest);
        final List<VehicleDto> vehicleDtos = vehicles.stream()
                .map(vehicle -> new VehicleDto(vehicle.getVin()))
                .collect(Collectors.toList());

        return PageCreator.of(vehicles.getPageable(), vehicleDtos);
    }

    @Override
    public Page<FeatureDto> getFeatures(final String vin, final PageRequest pageRequest) {
        final Vehicle vehicle = findVehicleByVin(vin);
        final Page<Feature> featurePage = this.featureRepository.findAll(pageRequest);
        final List<Feature> features = featurePage.getContent();
        final Stream<Feature> installableFeatures =
                features.stream().filter(feature -> feature.getVehicles().contains(vehicle));
        final Stream<Feature> incompatibleFeatures =
                features.stream().filter(feature -> !feature.getVehicles().contains(vehicle));

        final List<FeatureDto> featureDtoList = new ArrayList<>();
        featureDtoList.addAll(createVehicleDtoList(incompatibleFeatures, NOT_INSTALLABLE));
        featureDtoList.addAll(createVehicleDtoList(installableFeatures, INSTALLABLE));

        return PageCreator.of(featurePage.getPageable(), featureDtoList);
    }

    @Override
    public Page<FeatureDto> getIncompatibleFeatures(final String vin, final PageRequest pageRequest) {
        final Vehicle vehicle = findVehicleByVin(vin);
        final Page<Feature> features = this.featureRepository.findFeatureByVehiclesIsNotContaining(vehicle, pageRequest);
        final List<FeatureDto> featureDtos = createVehicleDtoList(features, NOT_INSTALLABLE);

        return PageCreator.of(features.getPageable(), featureDtos);
    }

    @Override
    public Page<FeatureDto> getInstallableFeatures(final String vin, final PageRequest pageRequest) {
        final Vehicle vehicle = findVehicleByVin(vin);
        final Page<Feature> features = this.featureRepository.findFeatureByVehiclesIsContaining(vehicle, pageRequest);
        final List<FeatureDto> featureDtos = createVehicleDtoList(features, INSTALLABLE);

        return PageCreator.of(features.getPageable(), featureDtos);
    }

    private Vehicle findVehicleByVin(final String vehicle) {
        return this.vehicleRepository.findById(vehicle).orElseThrow(() -> new DataNotFoundException(
                String.format(VEHICLE_NOT_FOUND_MESSAGE, vehicle)));
    }

    private List<FeatureDto> createVehicleDtoList(Page<Feature> featurePage, Boolean isInstallable) {
        return createVehicleDtoList(featurePage.getContent().stream(), isInstallable);
    }

    private List<FeatureDto> createVehicleDtoList(Stream<Feature> features, Boolean isInstallable) {
        return features
                .map(feature -> FeatureToFeatureDtoConverter.converter(feature, isInstallable))
                .collect(Collectors.toList());
    }
}
