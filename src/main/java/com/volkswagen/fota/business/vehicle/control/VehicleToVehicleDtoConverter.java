package com.volkswagen.fota.business.vehicle.control;

import com.volkswagen.fota.business.vehicle.entity.Vehicle;

public final class VehicleToVehicleDtoConverter {

    private VehicleToVehicleDtoConverter(){}

    /**
     * Convert a Vehicle to VehicleDto. Is received a Boolean by parameter to set into the VehicleDto
     * the information if the Vehicle is installable or not.
     *
     * @param vehicle the vehicle parameter
     * @param isInstallable the isInstallable parameter
     * @return {@VehicleDto}
     */
    public static VehicleDto converter(final Vehicle vehicle, final Boolean isInstallable) {
        return VehicleDto.builder()
                .vin(vehicle.getVin())
                .installable(isInstallable)
                .incompatible(!isInstallable)
                .build();
    }
}
