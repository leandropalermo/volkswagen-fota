package com.volkswagen.fota.business.vehicle.boundary;

import com.volkswagen.fota.business.feature.control.FeatureDto;
import com.volkswagen.fota.business.vehicle.control.VehicleDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Api(value = "Vehicle")
@RequestMapping("/vehicles")
public interface VehicleController {

    @ApiOperation(value = "Find all features by VIN", notes = "gives all features that can/cannot be installed for the corresponding vin", response = Void.class, tags={ "vehicles", })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page.", defaultValue = "10"),
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Void.class),
            @ApiResponse(code = 404, message = "Not found", response = Void.class) })

    @GetMapping(value = "/{vin}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Page<FeatureDto>> getFeatures(@ApiParam(value = "Vehicle identification number", required=true ) @PathVariable("vin") String vin,
                                                 @RequestParam("page") int page, @RequestParam("size") int size);


    @ApiOperation(value = "Find incompatible features by VIN", notes = "gives all the features that cannot be installed for the corresponding vin", response = Void.class, tags={ "vehicles", })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page.", defaultValue = "10"),
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Void.class),
            @ApiResponse(code = 404, message = "Not found", response = Void.class) })

    @GetMapping(value = "/{vin}/incompatible", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Page<FeatureDto>> getIncompatible(@ApiParam(value = "Vehicle identification number",required=true ) @PathVariable("vin") String vin,
                                                     @RequestParam("page") int page, @RequestParam("size") int size);


    @ApiOperation(value = "Find installable features by VIN", notes = "gives all the features that can be installed for the corresponding vin", response = Void.class, tags={ "vehicles", })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page.", defaultValue = "10"),
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Void.class),
            @ApiResponse(code = 404, message = "Not found", response = Void.class) })

    @GetMapping(value = "/{vin}/installable", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Page<FeatureDto>> getInstallable(@ApiParam(value = "Vehicle identification number",required=true ) @PathVariable("vin") String vin,
                                                    @RequestParam("page") int page, @RequestParam("size") int size);


    @ApiOperation(value = "Find all vehicles", notes = "returns a list of all vehicles", response = Void.class, tags={ "vehicles", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Void.class) })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page.", defaultValue = "10"),
    })

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Page<VehicleDto>> getVehicles(@RequestParam("page") int page, @RequestParam("size") int size);
}
