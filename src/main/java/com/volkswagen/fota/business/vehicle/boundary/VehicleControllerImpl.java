package com.volkswagen.fota.business.vehicle.boundary;

import com.volkswagen.fota.business.feature.control.FeatureDto;
import com.volkswagen.fota.business.vehicle.control.VehicleDto;
import com.volkswagen.fota.business.vehicle.control.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VehicleControllerImpl implements VehicleController {

    @Autowired
    private VehicleService vehicleService;

    @Override
    public ResponseEntity<Page<FeatureDto>> getFeatures(final String vin,
                                                        final @RequestParam("page") int page,
                                                        final @RequestParam("size") int size) {
        final Page<FeatureDto> featurePage = this.vehicleService.getFeatures(vin, getPageRequest(page, size));
        return new ResponseEntity<>(featurePage, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Page<FeatureDto>> getIncompatible(final String vin,
                                                            final @RequestParam("page") int page,
                                                            final @RequestParam("size") int size) {
        final Page<FeatureDto> featurePage = this.vehicleService.getIncompatibleFeatures(vin, getPageRequest(page, size));
        return new ResponseEntity<>(featurePage, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Page<FeatureDto>> getInstallable(final String vin,
                                                           final @RequestParam("page") int page,
                                                           final @RequestParam("size") int size) {
        final Page<FeatureDto> featurePage = this.vehicleService.getInstallableFeatures(vin, getPageRequest(page, size));
        return new ResponseEntity<>(featurePage, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Page<VehicleDto>> getVehicles(final @RequestParam("page") int page,
                                                        final @RequestParam("size") int size) {
        final Page<VehicleDto> vehiclePage = this.vehicleService.getVehicles(getPageRequest(page, size));
        return new ResponseEntity<>(vehiclePage, HttpStatus.OK);
    }

    private PageRequest getPageRequest(@RequestParam("page") int page, @RequestParam("size") int size) {
        return PageRequest.of(page, size);
    }
}
