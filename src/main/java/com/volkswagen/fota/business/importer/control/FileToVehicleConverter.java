package com.volkswagen.fota.business.importer.control;

import com.volkswagen.fota.business.feature.entity.TypeFile;
import com.volkswagen.fota.business.vehicle.entity.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

@Component
public class FileToVehicleConverter {

    private static final String SPLIT_FILE_NAME = "_";

    private static final int SPLIT_FILE_NAME_INDEX = 0;

    @Autowired
    private FeatureTypeFileFactory featureTypeFileFactory;

    /**
     * @param filePath  the filePath parameter
     * @return a list o Vehicle
     * @throws IOException when converting the Path content to a List of Vehicle
     */
    public List<Vehicle> convert(final Path filePath) throws IOException {
        final String fileName = getFileName(filePath);
        final FeatureTypeFile featureTypeFile = getFeatureTypeFile(fileName);

        return featureTypeFile.convertPathContentToVehicleList(filePath);
    }

    /**
     * Get the specific FeatureTypeFile to the received file
     *
     * @param fileName the fileName parameter
     * @return a FeatureTypeFile instance
     */
    private FeatureTypeFile getFeatureTypeFile(String fileName) {
        final TypeFile typeFile = TypeFile.getTypeFileByValue(fileName);
        return this.featureTypeFileFactory.getFeatureTypeFile(typeFile);
    }

    /**
     * Get the filename from a Path.
     *
     * @param filePath
     * @return the file name
     */
    private String getFileName(Path filePath) {
        final String fileName = filePath.getFileName().toString();
        final String[] splitFileName = fileName.toUpperCase().split(SPLIT_FILE_NAME);

        return splitFileName[SPLIT_FILE_NAME_INDEX];
    }
}
