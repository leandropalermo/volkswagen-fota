package com.volkswagen.fota.business.importer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileRegister {

    @Id
    private UUID id;

    @Column(name = "file_name", nullable = false, unique = true)
    private String fileName;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    public FileRegister(final String fileName) {
        this.fileName = fileName;
    }

    @PrePersist
    public void generateId() {
        this.id = UUID.randomUUID();
    }
}
