package com.volkswagen.fota.business.importer.boundary;


import com.volkswagen.fota.business.importer.control.FileDataRegister;
import com.volkswagen.fota.business.importer.control.FileToVehicleConverter;
import com.volkswagen.fota.business.importer.control.FileValidator;
import com.volkswagen.fota.business.vehicle.entity.Vehicle;
import com.volkswagen.fota.exception.AlreadyInsertedException;
import com.volkswagen.fota.exception.FileFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

@Component
public class FeatureImporter {

    private final Logger LOGGER = LoggerFactory.getLogger(FeatureImporter.class);

    @Value("${csv.folder.path}")
    private String FOLDER_PATH;

    @Autowired
    private FileToVehicleConverter fileToVehicleConverter;

    @Autowired
    private FileValidator fileValidator;

    @Autowired
    private FileDataRegister fileDataRegister;

    /**
     * Start the importer process.
     * The process execution can be parameterized adjusting the value in application.properties file.
     *
     * @throws IOException
     */
    @Scheduled(cron = "${csv.importer.schedule}")
    public void importFeatures() throws IOException {
        LOGGER.info("Starting import process.");
        readFiles();
        LOGGER.info("Import process finalized.");
    }

    /**
     * Read all the CSV files that exists in the folder. This folder path is parameterized.
     *
     * @throws IOException
     */
    public void readFiles() throws IOException {
        try(Stream<Path> paths = Files.walk(Paths.get(FOLDER_PATH))) {
            paths.forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {
                    try {
                        this.fileValidator.validate(filePath);
                        List<Vehicle> vehicles = this.fileToVehicleConverter.convert(filePath);
                        if (!CollectionUtils.isEmpty(vehicles)) {
                            this.fileDataRegister.registerData(filePath, vehicles);
                        }
                    } catch (FileFormatException | AlreadyInsertedException e) {
                        LOGGER.info(e.getMessage());
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage());
                        LOGGER.debug(e.getStackTrace().toString());
                    }
                }
            });
        }
    }
}
