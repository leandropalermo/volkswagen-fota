package com.volkswagen.fota.business.importer.control;

import com.volkswagen.fota.business.feature.entity.TypeFile;
import com.volkswagen.fota.business.vehicle.entity.Vehicle;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface FeatureTypeFile {

    /**
     * This method return the type of the Feature that the implementation is associated:
     * <li>HARDWARE</li>
     * <li>SOFTWARE</li>
     *
     * @return {@TypeFile}
     */
    TypeFile getTypeFile();

    /**
     * Converts the {@Path} received by parameter, reading all lines that exists into the pathFile
     * to a {@List} of {@Vehicle}
     *
     * @param pathFile the pathFile parameter
     * @return {@List} of {@Vehicle}
     * @throws IOException when occurs some error while reading the lines from the pathFile.
     */
    List<Vehicle> convertPathContentToVehicleList(Path pathFile) throws IOException;
}
