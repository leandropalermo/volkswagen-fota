package com.volkswagen.fota.business.importer.control;

import com.volkswagen.fota.business.importer.entity.FileRegister;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface FileRegisterRepository extends JpaRepository<FileRegister, UUID> {
    Optional<FileRegister> findFileRegisterByFileName(String fileName);
}
