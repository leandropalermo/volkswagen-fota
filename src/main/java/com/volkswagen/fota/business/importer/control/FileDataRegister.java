package com.volkswagen.fota.business.importer.control;

import com.volkswagen.fota.business.importer.entity.FileRegister;
import com.volkswagen.fota.business.vehicle.control.VehicleRepository;
import com.volkswagen.fota.business.vehicle.entity.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.util.List;

@Service
public class FileDataRegister {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private FileRegisterRepository fileRegisterRepository;

    /**
     * Persist into the Database a list of vehicles and the information
     * from the file which the Vehicles data were obtained.
     *
     * @param filePath
     * @param vehicles
     */
    public void registerData(Path filePath, List<Vehicle> vehicles) {
        persistVehicles(vehicles);
        persistFileRegister(filePath);
    }

    /**
     * Persist the File information.
     *
     * @param filePath  the filePath parameter
     */
    private void persistFileRegister(Path filePath) {
        final FileRegister fileRegister = new FileRegister(filePath.getFileName().toString());
        this.fileRegisterRepository.save(fileRegister);
    }

    /**
     * Persist a list o Vehicles.
     *
     * @param vehicles
     */
    private void persistVehicles(List<Vehicle> vehicles) {
        this.vehicleRepository.saveAll(vehicles);
    }
}
