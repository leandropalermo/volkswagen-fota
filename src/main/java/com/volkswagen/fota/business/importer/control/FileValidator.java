package com.volkswagen.fota.business.importer.control;

import com.volkswagen.fota.business.importer.entity.FileRegister;
import com.volkswagen.fota.exception.AlreadyInsertedException;
import com.volkswagen.fota.exception.FileFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class FileValidator {

    private final Logger LOGGER = LoggerFactory.getLogger(FileValidator.class);

    public static final String CSV_PATTERN = ".*csv";
    public static final Pattern pattern = Pattern.compile(CSV_PATTERN);

    @Autowired
    private FileRegisterRepository fileRegisterRepository;

    /**
     * Check the File's format and if the File was previously registered.
     *
     * @param filePath the filePath parameter
     * @throws FileFormatException when the file is not a CSV type format.
     * @throws AlreadyInsertedException when the file has been already registered
     */
    public void validate(final Path filePath) throws FileFormatException, AlreadyInsertedException {
        final String fileName = filePath.getFileName().toString();
        LOGGER.info(String.format("Validating %s file", fileName));
        if (isNotCsvFile(fileName)) {
            LOGGER.info(String.format("File %s is not of csv file type.", fileName));
            throw new FileFormatException(String.format("File %s is not of csv file type.", fileName));
        }

        if (isAlreadyInsertedFile(fileName)) {
            LOGGER.info(String.format("File %s has already been registered.", fileName));
            throw new AlreadyInsertedException(String.format("File %s has already been registered.", fileName));
        }
    }

    /**
     * Check if the File's format.
     *
     * @param fileName the fileName parameter
     * @return boolean
     */
    private boolean isNotCsvFile(final String fileName) {
        Matcher matcher = pattern.matcher(fileName);
        return !matcher.matches();
    }

    /**
     * Check if the File was previously registered.
     * Finding into the Database by the file's name received by parameter.
     *
     * @param fileName the fileName
     * @return boolean
     */
    private boolean isAlreadyInsertedFile(final String fileName) {
        final Optional<FileRegister> fileRegister =
                this.fileRegisterRepository.findFileRegisterByFileName(fileName);
        return fileRegister.isPresent();
    }
}
