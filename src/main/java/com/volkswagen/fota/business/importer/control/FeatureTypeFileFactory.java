package com.volkswagen.fota.business.importer.control;

import com.volkswagen.fota.business.feature.entity.TypeFile;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class FeatureTypeFileFactory {

    private Map<TypeFile, FeatureTypeFile> featureTypeFileMap;

    public FeatureTypeFileFactory(Set<FeatureTypeFile> featureTypeFileSet) {
        setFeatureTypeFileMap(featureTypeFileSet);
    }

    /**
     * @param featureTypeFileSet
     */
    private void setFeatureTypeFileMap(Set<FeatureTypeFile> featureTypeFileSet) {
        featureTypeFileMap = new HashMap<>();
        featureTypeFileSet.forEach( featureTypeFile ->
                featureTypeFileMap.put(featureTypeFile.getTypeFile(), featureTypeFile));
    }

    /**
     * Get the FeatureTypeFile mapped to a specific TypeFile
     *
     * @param typeFile  the typeFile parameter
     * @return a instance of FeatureTypeFile
     */
    public FeatureTypeFile getFeatureTypeFile(final TypeFile typeFile) {
        return featureTypeFileMap.get(typeFile);
    }
}
