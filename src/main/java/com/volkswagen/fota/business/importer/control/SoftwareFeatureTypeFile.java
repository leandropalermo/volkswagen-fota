package com.volkswagen.fota.business.importer.control;

import com.volkswagen.fota.business.feature.entity.Feature;
import com.volkswagen.fota.business.feature.entity.Software;
import com.volkswagen.fota.business.feature.entity.TypeFile;
import com.volkswagen.fota.business.vehicle.entity.Vehicle;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class SoftwareFeatureTypeFile extends AbstractFeatureTypeFile implements FeatureTypeFile {

    @Override
    public TypeFile getTypeFile() {
        return TypeFile.SOFTWARE;
    }

    @Override
    @Transactional
    public List<Vehicle> convertPathContentToVehicleList(final Path pathFile) throws IOException {
        List<String> fileList = Files.readAllLines(pathFile);

        List<Vehicle> vehicles = convertFileListToVehicleList(pathFile, fileList);
        return validateVehiclesToBeRegistered(vehicles);
    }

    @Override
    protected  Vehicle convertCsvFileLineToVehicle(final String csvFileLine) {
        final String[] splitCsvFileLine = splitCsvLine(csvFileLine);
        final Software softwareFeature = new Software();
        softwareFeature.setFeature(splitCsvFileLine[FEATURE_FILE_NAME_INDEX]);
        final Set<Feature> features = new HashSet<>();
        features.add(softwareFeature);

        return createVehicle(splitCsvFileLine, features);

    }
}
