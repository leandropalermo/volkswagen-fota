package com.volkswagen.fota.business.importer.control;

import com.volkswagen.fota.business.feature.entity.Feature;
import com.volkswagen.fota.business.vehicle.control.VehicleRepository;
import com.volkswagen.fota.business.vehicle.entity.Vehicle;
import com.volkswagen.fota.exception.AlreadyInsertedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractFeatureTypeFile {

    @Autowired
    private VehicleRepository vehicleRepository;

    private final Logger LOGGER = LoggerFactory.getLogger(AbstractFeatureTypeFile.class);

    protected static final int VIN_FILE_NAME_INDEX = 0;
    protected static final int FEATURE_FILE_NAME_INDEX = 1;
    private static final String COMMA = ",";

    /**
     * Validate the List of Vehicles received by parameter,
     * cheching if theses vehicles have already been registered into the system.
     *
     * @param vehicles
     * @return
     */
    protected List<Vehicle> validateVehiclesToBeRegistered(List<Vehicle> vehicles) {
        Map<String, Vehicle> vehiclesConvertedMap = new HashMap<>();
        vehicles.forEach( vehicle -> {
            Feature feature = vehicle.getFeatures().stream().findFirst().get();
            String featureName = feature.getFeature();
            String vin = vehicle.getVin();
            try {
                isFeatureAlreadyAssociatedToVin(vin, featureName);
                Vehicle mappedVehicle = vehiclesConvertedMap.get(vin);
                if (Objects.isNull(mappedVehicle)) {
                    mappedVehicle = vehicle;
                    vehiclesConvertedMap.put(vin, mappedVehicle);
                }
                mappedVehicle.getFeatures().add(feature);
            } catch (AlreadyInsertedException e) {
                LOGGER.info(e.getMessage());
            }
        });
        return convertMapToList(vehiclesConvertedMap);
    }

    /**
     * Verify into the database if the feature received has association with the vin received
     *
     * @param vin
     * @param feature
     * @throws AlreadyInsertedException
     */
    @Transactional
    protected void isFeatureAlreadyAssociatedToVin(final String vin, final String feature) throws AlreadyInsertedException {
        Optional<Vehicle> vehicle = this.vehicleRepository.findById(vin);
        if (vehicle.isPresent()) {
            Optional<Feature> filteredFeature = vehicle.get().getFeatures().stream().filter(f -> f.getFeature().equals(feature)).findFirst();
            if (filteredFeature.isPresent()) {
                throw new AlreadyInsertedException(
                        String.format("The feature %s has already been associated to the VIN %s", feature, vin));
            }
        }
    }

    /**
     * Converting the line of the CSV file to Vehicle.
     *
     * @param csvFileLine
     * @return
     */
    protected abstract Vehicle convertCsvFileLineToVehicle(final String csvFileLine);

    /**
     * It'll be receiving a String
     * representing the file line. in this String must have a comma separating the first column (VIN)
     * from the second column (FEATURE)
     *
     * @param csvFileLine
     * @return
     */
    protected String[] splitCsvLine(String csvFileLine) {
        return csvFileLine.split(COMMA);
    }

    /**
     * I'll get all values added into the map, converting it to a List of Vehicle
     *
     * @param vehiclesConvertedMap
     * @return the {@List} of {@Vehicle}
     */
    protected List<Vehicle> convertMapToList(Map<String, Vehicle> vehiclesConvertedMap) {
        return vehiclesConvertedMap.values()
                .stream()
                .collect(Collectors.toList());
    }

    /**
     * Convert a list of Files to a list of Vehicles.
     *
     * @param filePath
     * @param fileList
     * @return the {@List} of {@Vehicle}
     */
    protected List<Vehicle> convertFileListToVehicleList(Path filePath, List<String> fileList) {
        List<Vehicle> vehicles = fileList.stream()
                .map(file -> convertCsvFileLineToVehicle(file))
                .collect(Collectors.toList());
        LOGGER.info(String.format("Was read %s lines from the File %s, converting a total of %s Vehicles",
                fileList.size(), filePath.getFileName().toString(), vehicles.size()));
        return vehicles;
    }

    /**
     * Create a new instance of Vehicle
     *
     * @param splitCsvFileLine
     * @param features
     * @return a new {@Vehicle}
     */
    protected Vehicle createVehicle(String[] splitCsvFileLine, Set<Feature> features) {
        final Vehicle vehicle = new Vehicle();
        vehicle.setVin(splitCsvFileLine[VIN_FILE_NAME_INDEX]);
        vehicle.setFeatures(features);
        return vehicle;
    }
}
