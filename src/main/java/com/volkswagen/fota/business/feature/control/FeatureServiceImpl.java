package com.volkswagen.fota.business.feature.control;

import com.volkswagen.fota.business.PageCreator;
import com.volkswagen.fota.business.feature.entity.Feature;
import com.volkswagen.fota.business.vehicle.control.VehicleDto;
import com.volkswagen.fota.business.vehicle.control.VehicleRepository;
import com.volkswagen.fota.business.vehicle.control.VehicleToVehicleDtoConverter;
import com.volkswagen.fota.business.vehicle.entity.Vehicle;
import com.volkswagen.fota.exception.DataNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FeatureServiceImpl implements FeatureService {

    @Autowired
    private FeatureRepository featureRepository;

    @Autowired
    private VehicleRepository vehicleRepository;

    private final Logger LOGGER = LoggerFactory.getLogger(FeatureServiceImpl.class);

    private static final String FEATURE_NOT_FOUND_MESSAGE = "Was not possible to find a Feature %s.";

    @Override
    public Page<FeatureDto> getAllFeatures(final PageRequest pageRequest) {
        final Page<Feature> features = this.featureRepository.findAll(pageRequest);
        final List<FeatureDto> featureDtos = features.stream()
                .map(feature -> new FeatureDto(feature)).collect(Collectors.toList());

        return PageCreator.of(features.getPageable(), featureDtos);
    }

    @Override
    public Page<VehicleDto> getAllVin(final String feature, final PageRequest pageRequest) {
        final Feature featureRequested = findFeatureById(feature);
        final Page<Vehicle> vehiclePage = this.vehicleRepository.findAll(pageRequest);

        final List<Vehicle> vehicles = vehiclePage.getContent();
        final Stream<Vehicle> incompatibleVehicles =
                vehicles.stream().filter(v -> !v.getFeatures().contains(featureRequested));
        final Stream<Vehicle> installableVehicles =
                vehicles.stream().filter(v -> v.getFeatures().contains(featureRequested));

        final List<VehicleDto> vehicleDtoList = new ArrayList<>();
        vehicleDtoList.addAll(createVehicleDtoList(incompatibleVehicles, Boolean.FALSE));
        vehicleDtoList.addAll(createVehicleDtoList(installableVehicles, Boolean.TRUE));
        LOGGER.debug(String.format("Quantity of Vehicles found was %s. Quantity of Vechicles being sent in response %s.",
                vehicles.size(), vehicleDtoList.size()));

        return PageCreator.of(vehiclePage.getPageable(), vehicleDtoList);
    }

    @Override
    public Page<VehicleDto> getVinIncompatible(String feature, final PageRequest pageRequest) {
        final Feature featureRequested = findFeatureById(feature);
        final Page<Vehicle> vehiclePage =
                this.vehicleRepository.findVehicleByFeaturesIsNotContaining(featureRequested, pageRequest);
        final List<VehicleDto> vehicleDtoList = createVehicleDtoList(vehiclePage, Boolean.FALSE);

        return PageCreator.of(vehiclePage.getPageable(), vehicleDtoList);
    }

    @Override
    public Page<VehicleDto> getVinInstallable(final String feature, final PageRequest pageRequest) {
        final Feature featureRequested = findFeatureById(feature);
        final Page<Vehicle> vehiclesPage = this.vehicleRepository.findVehicleByFeaturesContaining(featureRequested, pageRequest);
        final List<VehicleDto> vehicleDtoList = createVehicleDtoList(vehiclesPage, Boolean.TRUE);

        return PageCreator.of(vehiclesPage.getPageable(), vehicleDtoList);
    }

    private Feature findFeatureById(String feature) {
        return this.featureRepository.findById(feature).orElseThrow(() -> new DataNotFoundException(
                String.format(FEATURE_NOT_FOUND_MESSAGE, feature)));
    }

    private List<VehicleDto> createVehicleDtoList(Page<Vehicle> vehiclePage, Boolean isInstallable) {
        final List<Vehicle> vehicles = vehiclePage.getContent();
        return createVehicleDtoList(vehicles.stream(), isInstallable);
    }

    private List<VehicleDto> createVehicleDtoList(Stream<Vehicle> vehicles, Boolean isInstallable) {

        return vehicles
                .map(vehicle -> VehicleToVehicleDtoConverter.converter(vehicle, isInstallable))
                .collect(Collectors.toList());
    }
}
