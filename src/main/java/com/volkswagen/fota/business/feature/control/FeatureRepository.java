package com.volkswagen.fota.business.feature.control;

import com.volkswagen.fota.business.feature.entity.Feature;
import com.volkswagen.fota.business.vehicle.entity.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeatureRepository extends JpaRepository<Feature, String> {

    Page<Feature> findFeatureByVehiclesIsContaining(Vehicle vehicle, Pageable pageable);
    Page<Feature> findFeatureByVehiclesIsNotContaining(Vehicle vehicle, Pageable pageable);
}
