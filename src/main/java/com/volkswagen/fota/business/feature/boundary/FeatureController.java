package com.volkswagen.fota.business.feature.boundary;

import com.volkswagen.fota.business.feature.control.FeatureDto;
import com.volkswagen.fota.business.vehicle.control.VehicleDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Api(value = "feature")
@RequestMapping("/features")
public interface FeatureController {

    @ApiOperation(value = "Find all features", notes = "returns a list of all feature codes", response = Void.class, tags={ "features", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Page.class) })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page.", defaultValue = "10"),
    })

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Page<FeatureDto>> getAllFeatures(@RequestParam("page") int page, @RequestParam("size") int size);

    @ApiOperation(value = "Find all VIN's by feature code", notes = "gives all the vins that can install the corresponding feature", response = Void.class, tags={ "features", })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page.", defaultValue = "10"),
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Page.class),
            @ApiResponse(code = 404, message = "Not found", response = Void.class) })

    @GetMapping(value = "/{feature}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Page<VehicleDto>> getAllVin(
            @ApiParam(value = "Correspondent feature code",required=true ) @PathVariable("feature") String feature,
            @RequestParam("page") int page, @RequestParam("size") int size);

    @ApiOperation(value = "Find incompatible VIN's by feature code", notes = "gives all the vins that cannot install the corresponding feature", response = Void.class, tags={ "features", })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page.", defaultValue = "10"),
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Page.class),
            @ApiResponse(code = 404, message = "Not found", response = Void.class) })

    @GetMapping(value = "/{feature}/incompatible", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Page<VehicleDto>> getVinIncompatible(
            @ApiParam(value = "Correspondent feature code",required=true ) @PathVariable("feature") String feature,
            @RequestParam("page") int page, @RequestParam("size") int size);

    @ApiOperation(value = "Find installable VIN's by feature code", notes = "gives all the vins that can install the corresponding feature", response = Void.class, tags={ "features", })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page.", defaultValue = "10"),
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Page.class),
            @ApiResponse(code = 404, message = "Not found", response = Void.class) })

    @GetMapping(value = "/{feature}/installable", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Page<VehicleDto>> getVinInstallable(
            @ApiParam(value = "Correspondent feature code",required=true ) @PathVariable("feature") String feature,
            @RequestParam("page") int page, @RequestParam("size") int size);

}
