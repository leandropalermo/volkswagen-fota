package com.volkswagen.fota.business.feature.control;

import com.volkswagen.fota.business.feature.entity.Feature;

public class FeatureToFeatureDtoConverter {

    /**
     * Convert a Feature to FeatureDto.
     * Is received a Boolean by parameter to set into the FeatureDto
     * the information if the Feature is installable or not.
     *
     * @param feature the feature parameter
     * @param isInstallable the isInstallable parameter
     * @return {@FeatureDto}
     */
    public static FeatureDto converter(final Feature feature, final Boolean isInstallable) {
        return FeatureDto.builder()
                .feature(feature.getFeature())
                .installable(isInstallable)
                .incompatible(!isInstallable)
                .featureType(feature.getTypeFile().name())
                .build();
    }
}
