package com.volkswagen.fota.business.feature.control;

import com.volkswagen.fota.business.vehicle.control.VehicleDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface FeatureService {

    /**
     * Find All Feature registered into the database.
     *
     * @param pageRequest the pageRequest parameter
     * @return {@Page} of {@FeatureDto}
     */
    Page<FeatureDto> getAllFeatures(final PageRequest pageRequest);

    /**
     * Find all Vin associated to a specific Feature received by parameter.
     *
     * @param feature the feature parameter
     * @param pageRequest the pageRequest parameter
     * @return [@Page} of {@VehicleDto}
     */
    Page<VehicleDto> getAllVin(final String feature, final PageRequest pageRequest);

    /**
     * Find all incompatible Vin to a specific Feature received by parameter.
     * It looks for all Vehicles who do not have any association to the Feature.
     *
     * @param feature the feature parameter
     * @param pageRequest the pageRequest paramter
     * @return [@Page} of {@VehicleDto}
     */
    Page<VehicleDto> getVinIncompatible(final String feature, final PageRequest pageRequest);

    /**
     * Find all installable Vin to a specific Feature received by parameter.
     * It looks for all Vehicles who have some association to the Feature.
     *
     * @param feature the feature parameter
     * @param pageRequest the pageRequest paramter
     * @return [@Page} of {@VehicleDto}
     */
    Page<VehicleDto> getVinInstallable(final String feature, final PageRequest pageRequest);
}
