package com.volkswagen.fota.business.feature.boundary;

import com.volkswagen.fota.business.feature.control.FeatureDto;
import com.volkswagen.fota.business.feature.control.FeatureService;
import com.volkswagen.fota.business.vehicle.control.VehicleDto;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeatureControllerImpl implements FeatureController {

    @Autowired
    private FeatureService featureService;

    public ResponseEntity<Page<FeatureDto>> getAllFeatures(
            @RequestParam("page") int page, @RequestParam("size") int size) {
        final Page<FeatureDto> features = this.featureService.getAllFeatures(getPageRequest(page, size));
        return new ResponseEntity<>(features, HttpStatus.OK);
    }

    public ResponseEntity<Page<VehicleDto>> getAllVin(
            @ApiParam(value = "Correspondent feature code",required=true ) @PathVariable("feature") String feature,
            @RequestParam("page") int page, @RequestParam("size") int size) {
        final Page<VehicleDto> vehicles = this.featureService.getAllVin(feature, getPageRequest(page, size));
        return new ResponseEntity<>(vehicles, HttpStatus.OK);
    }

    public ResponseEntity<Page<VehicleDto>> getVinIncompatible(
            @ApiParam(value = "Correspondent feature code",required=true ) @PathVariable("feature") String feature,
            @RequestParam("page") int page, @RequestParam("size") int size) {
        final Page<VehicleDto> featurePage = this.featureService.getVinIncompatible(feature, getPageRequest(page, size));
        return new ResponseEntity<>(featurePage, HttpStatus.OK);
    }

    public ResponseEntity<Page<VehicleDto>> getVinInstallable(
            @ApiParam(value = "Correspondent feature code",required=true ) @PathVariable("feature") String feature,
            @RequestParam("page") int page, @RequestParam("size") int size) {
        final Page<VehicleDto> vehiclePage = this.featureService.getVinInstallable(feature, getPageRequest(page, size));
        return new ResponseEntity<>(vehiclePage, HttpStatus.OK);
    }

    private PageRequest getPageRequest(@RequestParam("page") int page, @RequestParam("size") int size) {
        return PageRequest.of(page, size);
    }
}
