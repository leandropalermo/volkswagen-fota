package com.volkswagen.fota.business.feature.control;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.volkswagen.fota.business.feature.entity.Feature;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FeatureDto {

    private String feature;

    private String vin;

    private String featureType;

    private Boolean incompatible;

    private Boolean installable;

    public FeatureDto(final Feature feature) {
        if (Objects.isNull(feature)) {
            throw new IllegalArgumentException("Argument passed into the constructor is null");
        }
        this.feature = feature.getFeature();
        this.featureType = feature.getTypeFile().name();
    }
}
