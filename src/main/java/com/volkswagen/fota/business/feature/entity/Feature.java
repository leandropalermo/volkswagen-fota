package com.volkswagen.fota.business.feature.entity;

import com.volkswagen.fota.business.vehicle.entity.Vehicle;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;


@Entity
@DiscriminatorColumn(name="feature_type", discriminatorType = DiscriminatorType.STRING)
@Data
public class Feature {

    @Id
    private String feature;

    @ManyToMany(targetEntity = Vehicle.class, mappedBy = "features", cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude @ToString.Exclude
    private Set<Vehicle> vehicles;

    @Column(name = "feature_type", insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private TypeFile typeFile;

}

