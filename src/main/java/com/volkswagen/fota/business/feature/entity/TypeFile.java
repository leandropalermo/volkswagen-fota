package com.volkswagen.fota.business.feature.entity;

import java.util.Arrays;

public enum TypeFile {

    SOFTWARE("SOFT"),
    HARDWARE("HARD");

    private String typeFile;

    TypeFile(String typeFile) {
        this.typeFile = typeFile;
    }

    public String getTypeFile() {
        return this.typeFile;
    }

    public static TypeFile getTypeFileByValue(String typeFileValue) {
        return Arrays.stream(TypeFile.values())
                .filter(tf -> tf.getTypeFile().equalsIgnoreCase(typeFileValue))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
