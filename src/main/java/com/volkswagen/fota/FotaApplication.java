package com.volkswagen.fota;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class FotaApplication {

    public static void main(String[] args) {
        new SpringApplication(FotaApplication.class).run(args);
    }
}
