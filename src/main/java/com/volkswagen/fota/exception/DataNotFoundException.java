package com.volkswagen.fota.exception;

public class DataNotFoundException extends RuntimeException {

    public DataNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
