package com.volkswagen.fota.exception;

public class FileFormatException extends Exception {

    public FileFormatException(final String message) {
        super(message);
    }
}
