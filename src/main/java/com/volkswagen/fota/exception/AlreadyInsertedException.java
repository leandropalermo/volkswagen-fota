package com.volkswagen.fota.exception;

public class AlreadyInsertedException extends Exception {
    public AlreadyInsertedException(final String message) {
        super(message);
    }
}
