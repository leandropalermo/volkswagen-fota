package com.volkswagen.fota.business.vehicle.boundary;

import com.volkswagen.fota.business.feature.entity.Feature;
import com.volkswagen.fota.business.feature.entity.Software;
import com.volkswagen.fota.business.vehicle.control.VehicleRepository;
import com.volkswagen.fota.business.vehicle.entity.Vehicle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VehicleControllerIT {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private VehicleRepository vehicleRepository;

    private static final String VEHICLES_PATH = "/vehicles";
    private static final String INCOMPATIBLE_PATH = "/incompatible";
    private static final String INSTALLABLE_PATH = "/installable";
    private static final String VIN_PATH_VARIABLE = "/{vin}";
    private static final String PAGE_PARAMS = "?page={page}&size={size}";
    private static final String PAGE_NUMBER = "0";
    private static final String SIZE_PAGE_NUMBER = "10";

    private static final String FEATURE_1 = "FEATURE_1";
    private static final String VEHICLE_1 = "FEATURE_1";
    private static final String VIN_NOT_REGISTRED = "VIN_NOT_REGISTRED";

    private static boolean isInitialized = false;

    @Before
    public void initDatabase() {
        if (!isInitialized) {
            List<Vehicle> vehicles = createFeatureList();
            this.vehicleRepository.saveAll(vehicles);
            isInitialized = true;
        }
    }

    @Test
    public void whenGettingAllVehicles_thenReturnResponseStatusOk() {
        final String uri = new StringBuilder(VEHICLES_PATH)
                .append(PAGE_PARAMS)
                .toString();
        final ResponseEntity<String> response = this.testRestTemplate.getForEntity(uri, String.class,
                PAGE_NUMBER, SIZE_PAGE_NUMBER);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void whenGettingAllFeatureByVin_thenReturnResponseStatusOk() {
        final String uri = new StringBuilder(VEHICLES_PATH)
                .append(VIN_PATH_VARIABLE)
                .append(PAGE_PARAMS)
                .toString();
        final ResponseEntity<String> response = this.testRestTemplate.getForEntity(uri, String.class,
                VEHICLE_1, PAGE_NUMBER, SIZE_PAGE_NUMBER);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void whenVinWasNotFoundGettingAllFeaturesByVin_thenReturnResponseStatusNotFound() {
        String uri = new StringBuilder(VEHICLES_PATH)
                .append(VIN_PATH_VARIABLE)
                .append(PAGE_PARAMS)
                .toString();
        final ResponseEntity<Void> response = this.testRestTemplate.getForEntity(uri, Void.class,
                VIN_NOT_REGISTRED, PAGE_NUMBER, SIZE_PAGE_NUMBER);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void whenGettingAllIncompatibleFeatures_thenReturnResponseStatusOk() {
        String uri = new StringBuilder(VEHICLES_PATH)
                .append(VIN_PATH_VARIABLE)
                .append(INCOMPATIBLE_PATH)
                .append(PAGE_PARAMS)
                .toString();
        final ResponseEntity<Void> response = this.testRestTemplate.getForEntity(uri, Void.class,
                VEHICLE_1, PAGE_NUMBER, SIZE_PAGE_NUMBER);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void whenVinWasNotFoundGettingAllIncompatibleFeatures_thenReturnResponseStatusNotFound() {
        String uri = new StringBuilder(VEHICLES_PATH)
                .append(VIN_PATH_VARIABLE)
                .append(INCOMPATIBLE_PATH)
                .append(PAGE_PARAMS)
                .toString();
        final ResponseEntity<Void> response = this.testRestTemplate.getForEntity(uri, Void.class,
                VIN_NOT_REGISTRED, PAGE_NUMBER, SIZE_PAGE_NUMBER);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void whenGettingAllInstallableFeatures_thenReturnResponseStatusOk() {
        String uri = new StringBuilder(VEHICLES_PATH)
                .append(VIN_PATH_VARIABLE)
                .append(INSTALLABLE_PATH)
                .append(PAGE_PARAMS)
                .toString();
        final ResponseEntity<Void> response = this.testRestTemplate.getForEntity(uri, Void.class,
                VEHICLE_1, PAGE_NUMBER, SIZE_PAGE_NUMBER);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void whenVinWasNotFoundGettingAllInstallableFeatures_thenReturnResponseStatusNotFound() {
        String uri = new StringBuilder(VEHICLES_PATH)
                .append(VIN_PATH_VARIABLE)
                .append(INSTALLABLE_PATH)
                .append(PAGE_PARAMS)
                .toString();
        final ResponseEntity<Void> response = this.testRestTemplate.getForEntity(uri, Void.class,
                VIN_NOT_REGISTRED, PAGE_NUMBER, SIZE_PAGE_NUMBER);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    private List<Vehicle> createFeatureList() {
        Vehicle vehicle = createVehicle(VEHICLE_1);
        Feature feature = createFeature(FEATURE_1);
        vehicle.setFeatures(Collections.singleton(feature));

        return Arrays.asList(vehicle);
    }

    private Feature createFeature(final String featureName) {
        Software feature = new Software();
        feature.setFeature(featureName);
        return feature;
    }

    private Vehicle createVehicle(final String vin) {
        Vehicle vehicle = new Vehicle();
        vehicle.setVin(vin);
        return vehicle;
    }
}
