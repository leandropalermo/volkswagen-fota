package com.volkswagen.fota.business.importer.control;

import com.volkswagen.fota.business.feature.entity.TypeFile;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class FeatureTypeFileFactoryTest {

    private FeatureTypeFileFactory featureTypeFileFactory;

    @Before
    public void setup() {
        Set<FeatureTypeFile> featureTypeFiles = new HashSet<>();
        featureTypeFiles.add(new SoftwareFeatureTypeFile());
        featureTypeFiles.add(new HardwareFeatureTypeFile());

        this.featureTypeFileFactory = new FeatureTypeFileFactory(featureTypeFiles);
    }

    @Test
    public void whenPassedSoftwareTypeFile_thenReturnSoftwareFeatureTypeFileInstance() {
        FeatureTypeFile featureTypeFile = this.featureTypeFileFactory.getFeatureTypeFile(TypeFile.SOFTWARE);
        Assert.assertTrue(featureTypeFile instanceof  SoftwareFeatureTypeFile);
    }

    @Test
    public void whenPassedHardwareTypeFile_thenReturnHardwareFeatureTypeFileInstance() {
        FeatureTypeFile featureTypeFile = this.featureTypeFileFactory.getFeatureTypeFile(TypeFile.HARDWARE);
        Assert.assertTrue(featureTypeFile instanceof  HardwareFeatureTypeFile);
    }

    @Test
    public void whenPassedAllTypeFileExistent_thenAlwaysReturnSomeFeatureTypeFileInstance() {
        Arrays.stream(TypeFile.values())
              .forEach(typeFile -> {
                  FeatureTypeFile featureTypeFile = this.featureTypeFileFactory.getFeatureTypeFile(typeFile);
                  Assert.assertNotNull(featureTypeFile);
        });
    }
}
