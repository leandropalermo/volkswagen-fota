package com.volkswagen.fota.business.importer.control;

import com.volkswagen.fota.business.feature.entity.Feature;
import com.volkswagen.fota.business.feature.entity.Hardware;
import com.volkswagen.fota.business.feature.entity.TypeFile;
import com.volkswagen.fota.business.vehicle.control.VehicleRepository;
import com.volkswagen.fota.business.vehicle.entity.Vehicle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class HardwareFeatureTypeFileTest {

    @InjectMocks
    private HardwareFeatureTypeFile hardwareFeatureTypeFile;

    @Mock
    private VehicleRepository vehicleRepository;

    private Vehicle vehicle;
    private Feature feature;

    @Before
    public void setup()  {
        MockitoAnnotations.initMocks(this);

        this.feature = new Feature();
        this.feature.setFeature("FEATURE");
        this.feature.setTypeFile(TypeFile.HARDWARE);

        this.vehicle = new Vehicle();
        this.vehicle.setVin("VIN");
        this.vehicle.setFeatures(Collections.singleton(feature));
    }

    @Test
    public void whenConvertPathContentToVehicleList_thenReturnVehicleWithHardwareFeatureInstance() throws IOException {
        final Path csvFile = Paths.get("src","test","resources", "file.csv");
        List<Vehicle> vehicles = this.hardwareFeatureTypeFile.convertPathContentToVehicleList(csvFile);

        Assert.assertTrue(vehicles.size() > 0);
        Assert.assertTrue(vehicles.get(0).getFeatures().size() > 0);
        Assert.assertTrue(vehicles.get(0).getFeatures().stream().allMatch(f -> f instanceof Hardware));
    }

    @Test
    public void whenVinAlreadyRegistered_thenReturnEmptyList() throws IOException {
        Mockito.doReturn(Optional.of(this.vehicle)).when(this.vehicleRepository).findById(this.vehicle.getVin());

        final Path csvFile = Paths.get("src","test","resources", "file.csv");
        List<Vehicle> vehicles = this.hardwareFeatureTypeFile.convertPathContentToVehicleList(csvFile);

        Assert.assertEquals(0, vehicles.size());
    }

    @Test
    public void whenGetTypeFile_thenReturnSotwareType() {
        TypeFile typeFile = this.hardwareFeatureTypeFile.getTypeFile();

        Assert.assertEquals(TypeFile.HARDWARE, typeFile);
    }
}
