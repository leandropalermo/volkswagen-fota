package com.volkswagen.fota.business.importer.control;

import com.volkswagen.fota.business.importer.entity.FileRegister;
import com.volkswagen.fota.exception.AlreadyInsertedException;
import com.volkswagen.fota.exception.FileFormatException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@SpringBootTest
public class FileValidatorTest {

    @InjectMocks
    private FileValidator fileValidator;

    @Mock
    private FileRegisterRepository fileRegisterRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = FileFormatException.class)
    public void whenIsNotCsvFileType_thenThrowException() throws AlreadyInsertedException, FileFormatException {
        final Path csvFile = Paths.get("src","test","resources");
        fileValidator.validate(csvFile);
    }

    @Test(expected = AlreadyInsertedException.class)
    public void whenFileAlreadyInserted_thenThrowException() throws AlreadyInsertedException, FileFormatException {
        FileRegister fileRegister = new FileRegister();
        Mockito.doReturn(Optional.of(fileRegister)).when(this.fileRegisterRepository).findFileRegisterByFileName(Mockito.anyString());
        final Path csvFile = Paths.get("src","test","resources", "file.csv");
        fileValidator.validate(csvFile);
    }

    @Test
    public void whenFileHasNotBeenInsertedAndIsCsvType_thenDoNotThrowAnyException() throws AlreadyInsertedException, FileFormatException {
        final Path csvFile = Paths.get("src","test","resources", "file.csv");
        fileValidator.validate(csvFile);
    }
}
