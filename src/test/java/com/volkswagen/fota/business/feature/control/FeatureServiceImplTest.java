package com.volkswagen.fota.business.feature.control;

import com.volkswagen.fota.business.feature.entity.Feature;
import com.volkswagen.fota.business.feature.entity.TypeFile;
import com.volkswagen.fota.business.vehicle.control.VehicleDto;
import com.volkswagen.fota.business.vehicle.control.VehicleRepository;
import com.volkswagen.fota.business.vehicle.entity.Vehicle;
import com.volkswagen.fota.exception.DataNotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@SpringBootTest
public class FeatureServiceImplTest {

    @InjectMocks
    private FeatureServiceImpl featureService;

    @Mock
    private FeatureRepository featureRepository;

    @Mock
    private VehicleRepository vehicleRepository;


    private PageRequest  pageRequest = PageRequest.of(0, 10);
    private Feature feature;
    private Feature feature2;
    private Vehicle vehicle;
    private Vehicle vehicle2;
    private Set<Vehicle> vehiclesSet;
    private Set<Feature> featureSet;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.vehicle = new Vehicle();
        this.vehicle.setVin("VIN");

        this.vehicle2 = new Vehicle();
        this.vehicle2.setVin("VIN2");

        this.vehiclesSet = new HashSet<>();
        this.vehiclesSet.add(this.vehicle);
        this.vehiclesSet.add(this.vehicle2);

        this.feature = new Feature();
        this.feature.setFeature("FEATURE");
        this.feature.setTypeFile(TypeFile.SOFTWARE);
        this.feature.setVehicles(this.vehiclesSet);

        this.feature2 = new Feature();
        this.feature2.setFeature("FEATURE_2");
        this.feature2.setTypeFile(TypeFile.HARDWARE);
        this.feature2.setVehicles(Collections.singleton(this.vehicle));

        this.featureSet = new HashSet<>();
        this.featureSet.add(this.feature);
        this.featureSet.add(this.feature2);
        this.vehicle.setFeatures(featureSet);
        this.vehicle2.setFeatures(Collections.singleton(this.feature));

        Mockito.doReturn(Optional.ofNullable(this.feature)).when(this.featureRepository).findById(this.feature.getFeature());
        Mockito.doReturn(Optional.ofNullable(this.feature2)).when(this.featureRepository).findById(this.feature2.getFeature());
    }

    @Test
    public void whenGetAllFeature_thenReturnAllTheFeaturesWithinPageContent() {
        final List<Feature> features = Arrays.asList(feature);
        Page<Feature> featurePage =  new PageImpl<>(features, this.pageRequest, features.size());
        Mockito.doReturn(featurePage).when(this.featureRepository).findAll(this.pageRequest);

        Page<FeatureDto> pageReturned = this.featureService.getAllFeatures(this.pageRequest);

        Assert.assertEquals(featurePage.getContent().size(), pageReturned.getContent().size());
        Assert.assertEquals(featurePage.getNumber(), pageReturned.getNumber());
        Assert.assertEquals(featurePage.getTotalPages(), pageReturned.getTotalPages());
    }

    @Test
    public void whenGetAllVin_thenReturnAllVinWithingPageContent() {
        Page<Vehicle> vehiclePage =  new PageImpl<>(new ArrayList<>(this.vehiclesSet), this.pageRequest, this.vehiclesSet.size());
        Mockito.doReturn(vehiclePage).when(this.vehicleRepository).findAll(this.pageRequest);

        Page<VehicleDto> pageReturned = this.featureService.getAllVin(this.feature.getFeature(), pageRequest);

        Assert.assertEquals(vehiclePage.getContent().size(), pageReturned.getContent().size());
        Assert.assertEquals(vehiclePage.getNumber(), pageReturned.getNumber());
        Assert.assertEquals(vehiclePage.getTotalPages(), pageReturned.getTotalPages());
    }

    @Test
    public void whenGetVinIncompatible_thenReturnAllIncompatibleVinToTheFeature() {
        List<Vehicle> vehicles = Arrays.asList(this.vehicle2);
        Page<Vehicle> vehiclePage =  new PageImpl<>(vehicles, this.pageRequest, vehicles.size());
        Mockito.doReturn(vehiclePage).when(this.vehicleRepository).findVehicleByFeaturesIsNotContaining(this.feature2, this.pageRequest);

        Page<VehicleDto> pageReturned = this.featureService.getVinIncompatible(this.feature2.getFeature(), pageRequest);
        Assert.assertEquals(1, pageReturned.getContent().size());
        Assert.assertEquals(vehiclePage.getNumber(), pageReturned.getNumber());
        Assert.assertEquals(vehiclePage.getTotalPages(), pageReturned.getTotalPages());
    }

    @Test
    public void whenGetVinIncompatible_thenReturnAllInstallableVinToTheFeature() {
        List<Vehicle> vehicles = Arrays.asList(this.vehicle);
        Page<Vehicle> vehiclePage =  new PageImpl<>(vehicles, this.pageRequest, vehicles.size());
        Mockito.doReturn(vehiclePage).when(this.vehicleRepository).findVehicleByFeaturesContaining(this.feature2, this.pageRequest);

        Page<VehicleDto> pageReturned = this.featureService.getVinInstallable(this.feature2.getFeature(), pageRequest);
        Assert.assertEquals(1, pageReturned.getContent().size());
        Assert.assertEquals(vehiclePage.getNumber(), pageReturned.getNumber());
        Assert.assertEquals(vehiclePage.getTotalPages(), pageReturned.getTotalPages());
    }

    @Test(expected = DataNotFoundException.class)
    public void whenFeatureIsNotFound_thenThrowException() {
        this.featureService.getVinIncompatible("INVALID_FEATURE", pageRequest);
    }
}
