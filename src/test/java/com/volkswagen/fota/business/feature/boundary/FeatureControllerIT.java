package com.volkswagen.fota.business.feature.boundary;

import com.volkswagen.fota.business.feature.control.FeatureRepository;
import com.volkswagen.fota.business.feature.entity.Feature;
import com.volkswagen.fota.business.feature.entity.Software;
import com.volkswagen.fota.business.vehicle.entity.Vehicle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FeatureControllerIT {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private FeatureRepository featureRepository;

    private static final String FEATURES_PATH = "/features";
    private static final String INCOMPATIBLE_PATH = "/incompatible";
    private static final String INSTALLABLE_PATH = "/installable";
    private static final String FEATURE_PATH_VARIABLE = "/{feature}";
    private static final String PAGE_PARAMS = "?page={page}&size={size}";
    private static final String PAGE_NUMBER = "0";
    private static final String SIZE_PAGE_NUMBER = "10";

    private static final String FEATURE_1 = "FEATURE_1";
    private static final String VEHICLE_1 = "FEATURE_1";

    private static boolean isInitialized = false;

    @Before
    public void initDatabase() {
        if (!isInitialized) {
            List<Feature> features = createFeatureList();
            this.featureRepository.saveAll(features);
            isInitialized = true;
        }
    }

    @Test
    public void whenGettingAllFeatures_thenReturnResponseStatusOk() {
        String uri = new StringBuilder(FEATURES_PATH)
                .append(PAGE_PARAMS)
                .toString();
        final ResponseEntity<String> response = this.testRestTemplate.getForEntity(uri, String.class,
                PAGE_NUMBER, SIZE_PAGE_NUMBER);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void whenGettingAllVin_thenReturnResponseStatusOk() {
        String uri = new StringBuilder(FEATURES_PATH)
                .append(FEATURE_PATH_VARIABLE)
                .append(PAGE_PARAMS).toString();
        final ResponseEntity<Void> response = this.testRestTemplate.getForEntity(uri, Void.class,
                FEATURE_1, PAGE_NUMBER, SIZE_PAGE_NUMBER);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void whenFeatureWasNotFoundGettingAllVin_thenReturnResponseStatusNotFound() {
        String uri = new StringBuilder(FEATURES_PATH)
                .append(FEATURE_PATH_VARIABLE)
                .append(PAGE_PARAMS).toString();
        final ResponseEntity<Void> response = this.testRestTemplate.getForEntity(uri, Void.class,
                "feature", PAGE_NUMBER, SIZE_PAGE_NUMBER);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void whenGettingAllIncompatibleVin_thenReturnResponseStatusOk() {
        String uri = new StringBuilder(FEATURES_PATH)
                .append(FEATURE_PATH_VARIABLE)
                .append(INCOMPATIBLE_PATH)
                .append(PAGE_PARAMS)
                .toString();
        final ResponseEntity<Void> response = this.testRestTemplate.getForEntity(uri, Void.class,
                FEATURE_1, PAGE_NUMBER, SIZE_PAGE_NUMBER);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void whenFeatureWasNotFoundGettingAllIncompatibleVin_thenReturnResponseStatusNotFound() {
        String uri = new StringBuilder(FEATURES_PATH)
                .append(FEATURE_PATH_VARIABLE)
                .append(INCOMPATIBLE_PATH)
                .append(PAGE_PARAMS)
                .toString();
        final ResponseEntity<Void> response = this.testRestTemplate.getForEntity(uri, Void.class,
                "feature", PAGE_NUMBER, SIZE_PAGE_NUMBER);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void whenGettingAllInstallableVin_thenReturnResponseStatusOk() {
        String uri = new StringBuilder(FEATURES_PATH)
                .append(FEATURE_PATH_VARIABLE)
                .append(INSTALLABLE_PATH)
                .append(PAGE_PARAMS)
                .toString();
        final ResponseEntity<Void> response = this.testRestTemplate.getForEntity(uri, Void.class,
                FEATURE_1, PAGE_NUMBER, SIZE_PAGE_NUMBER);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void whenFeatureWasNotFoundGettingAllInstallableVin_thenReturnResponseStatusNotFound() {
        String uri = new StringBuilder(FEATURES_PATH)
                .append(FEATURE_PATH_VARIABLE)
                .append(INSTALLABLE_PATH)
                .append(PAGE_PARAMS)
                .toString();
        final ResponseEntity<Void> response = this.testRestTemplate.getForEntity(uri, Void.class,
                "feature", PAGE_NUMBER, SIZE_PAGE_NUMBER);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    private List<Feature> createFeatureList() {
        Vehicle vehicle = createVehicle(VEHICLE_1);
        Feature feature = createFeature(FEATURE_1);
        feature.setVehicles(Collections.singleton(vehicle));

        return Arrays.asList(feature);
    }

    private Feature createFeature(final String featureName) {
        Software feature = new Software();
        feature.setFeature(featureName);
        return feature;
    }

    private Vehicle createVehicle(final String vin) {
        Vehicle vehicle = new Vehicle();
        vehicle.setVin(vin);
        return vehicle;
    }
}
