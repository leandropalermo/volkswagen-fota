# Fota Importer Process

=============================

### Introduction
    
   This project contains:
    
   - fota: This application has the responsibility of import the data from CSV files to Database, 
     and to expose the data to the client by a Rest service. 
     Swagger-url: http://localhost:8080/fota/api/v1/swagger-ui.html
   
   - Mysql: The application used the MysqlDB to persist the imported data.
   
   - Docker: Docker is being used to create a container to "fota" and "Mysql".
   
   This project is using Java 8, Spring Boot, Maven, Docker, Swagger, and Mysql.    

=============================

### Prerequisities

   Required software components to run the program:
    
   - Docker;
   - Docker Compose;
   - Maven.
   
=============================

### Building

##### Install the software prerequired.

  - Link with guide to install the Docker: https://docs.docker.com/install/ 
  - Link with guide to install the Docker: https://docs.docker.com/install/ 
  - Link with guide to install Maven: https://maven.apache.org//

##### Build the application

Inside the service folder, has a ".pom" file. Get into this folder and type the command './mvnw clean install -U".
This is a maven command that makes the maven download all the libraries necessary to create the jar and run the application (you need to be connected on the internet to work).

After successfully executing the mvn command, it should have created a folder named "target". Inside the folder, there will be a ".jar" file.

Ps. Executing the maven command, will run all the Unit Tests created. To avoid it, would be necessary to add "-DskipTests" to the command line.
Ps. To run the Integration Tests, is necessary to run the command "mvn failsafe:integration-test".
Ps. Both the unit tests and the integration tests have been created, but they are not fully covering the code. Was only 
created tests to cover the most crucial points of the application.

##### Start the application

To start the application you must be inside the application root folder (It is where contains the docker-compose file) and type "docker-compose up --build".
This commando is responsible to start the MysqlDB and the fota application. 

Ps. The "data" folder is the host of the files. It is at the root of the application. If you need to change the font for CSV files,
You will need to add the new path address to the docker composition file.
        volumes:
              - <new_path>:/tmp 

=============================

### Authors
   
   - Leandro Palermo
